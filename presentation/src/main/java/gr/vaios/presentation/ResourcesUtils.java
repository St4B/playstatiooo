package gr.vaios.presentation;

import java.lang.reflect.Field;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public class ResourcesUtils {

    public static int getResId(String resName, Class<?> cls) {
        try {
            Field idField = cls.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

}
