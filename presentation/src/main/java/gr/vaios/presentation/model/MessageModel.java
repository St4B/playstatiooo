package gr.vaios.presentation.model;

/**
 * Created by v.tsitsonis on 2/3/2018.
 */

public final class MessageModel {

    private final String mText;

    private final String mUser;

    public static final String sMyName = "Me";

    private MessageModel(Builder builder) {
        mText = builder.mText;
        mUser = builder.mUser;
    }

    public String getText() {
        return mText;
    }

    public String getUser() {
        return mUser;
    }

    public static final class Builder {

        private String mText;

        private String mUser;

        public Builder text(String text) {
            mText = text;
            return this;
        }

        public Builder user(String user) {
            mUser = user;
            return this;
        }

        public MessageModel build () {
            return new MessageModel(this);
        }

    }
}
