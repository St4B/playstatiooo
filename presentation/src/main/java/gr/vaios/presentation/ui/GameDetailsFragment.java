package gr.vaios.presentation.ui;

import static gr.vaios.presentation.ResourcesUtils.getResId;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikepenz.entypo_typeface_library.Entypo;
import com.mikepenz.iconics.IconicsDrawable;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gr.vaios.presentation.R;
import gr.vaios.presentation.model.GameModel;
import jp.wasabeef.blurry.internal.Blur;
import jp.wasabeef.blurry.internal.BlurFactor;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class GameDetailsFragment extends AppCompatDialogFragment {

    private static final String BUNDLE_KEY_GAME = "game";

    public static final String TAG = "GameDetails";

    @BindView(R.id.llDetailsContainer)
    protected LinearLayout mDetailsContainer;

    @BindView(R.id.btnClose)
    protected ImageButton mClose;

    @BindView(R.id.ivPic)
    protected ImageView mPicture;

    @BindView(R.id.tvTitle)
    protected TextView mTitle;

    @BindView(R.id.tvBronzeTrophy)
    protected TextView mBronzeTrophy;

    @BindView(R.id.tvSilverTrophy)
    protected TextView mSilverTrophy;

    @BindView(R.id.tvGoldenTrophy)
    protected TextView mGoldenTrophy;

    @BindView(R.id.tvBlueTrophy)
    protected TextView mBlueTrophy;

    @BindView(R.id.tvTotalTrophies)
    protected TextView mTotalTrophies;

    @BindView(R.id.pbPercentage)
    protected ProgressBar mProgress;

    @BindView(R.id.tvPercentage)
    protected TextView mPercentage;

    public static GameDetailsFragment newInstance(GameModel game) {
        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_KEY_GAME, game);
        GameDetailsFragment fragment = new GameDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GameModel game = getArguments().getParcelable(BUNDLE_KEY_GAME);

        mTitle.setText(game.getTitle());

        setUpGamePoster(game);

        Drawable closeIcon = new IconicsDrawable(getContext())
                .icon(Entypo.Icon.ent_cross)
                .color(getResources().getColor(R.color.white))
                .sizeDp(24);
        mClose.setImageDrawable(closeIcon);

        setUpTrophies(game);
        setUpProgress(game);
    }

    private void setUpGamePoster(GameModel game) {
        int resId = getResId(game.getImage(), R.drawable.class);

        if (resId != -1) {
            mPicture.setBackgroundResource(resId);

            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

            BlurFactor blurFactor = new BlurFactor();
            blurFactor.width = metrics.widthPixels;
            blurFactor.height = metrics.heightPixels;
            blurFactor.sampling = 1;
            blurFactor.radius = 25;
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), resId);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, blurFactor.width,
                    blurFactor.height, false);
            Bitmap blurredBitmap = Blur.of(getActivity(), scaledBitmap, blurFactor);
            mDetailsContainer.setBackground(new BitmapDrawable(blurredBitmap));
        }
    }

    private void setUpTrophies(GameModel game) {
        Drawable bronzeTrophy = createTrophyDrawable(R.color.bronze);
        mBronzeTrophy.setCompoundDrawables(bronzeTrophy, null, null, null);
        mBronzeTrophy.setText(game.getBronze().toPlainString());

        Drawable silverTrophy = createTrophyDrawable(R.color.silver);
        mSilverTrophy.setCompoundDrawables(silverTrophy, null, null, null);
        mSilverTrophy.setText(game.getSilver().toPlainString());

        Drawable goldenTrophy = createTrophyDrawable(R.color.golden);
        mGoldenTrophy.setCompoundDrawables(goldenTrophy, null, null, null);
        mGoldenTrophy.setText(game.getGolden().toPlainString());

        Drawable blueTrophy = createTrophyDrawable(R.color.blue);
        mBlueTrophy.setCompoundDrawables(blueTrophy, null, null, null);
        mBlueTrophy.setText(game.getBlue().toPlainString());
    }

    private void setUpProgress(GameModel game) {
        Drawable whiteTrophy = createTrophyDrawable(R.color.white);
        mTotalTrophies.setCompoundDrawables(whiteTrophy, null, null, null);

        BigDecimal gatheredTrophies = game.getBronze()
                .add(game.getSilver())
                .add(game.getGolden())
                .add(game.getBlue());

        mProgress.setProgress(game.getPercentage().intValue());
        mProgress.getProgressDrawable().setColorFilter(getResources().getColor(R.color.white),
                PorterDuff.Mode.SRC_IN);
        mTotalTrophies.setText(
                gatheredTrophies.toPlainString() + "/" + game.getTotal().toPlainString());

        mPercentage.setText(game.getPercentage().toPlainString() + "%");
    }

    private Drawable createTrophyDrawable(int colorId) {
        return new IconicsDrawable(getContext())
                .icon(Entypo.Icon.ent_trophy)
                .color(getResources().getColor(colorId))
                .sizeDp(24);
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        return dialog;
    }

    @OnClick(R.id.btnClose)
    protected void close() {
        dismiss();
    }

}
