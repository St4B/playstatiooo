package gr.vaios.presentation.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mikepenz.entypo_typeface_library.Entypo;
import com.mikepenz.iconics.IconicsDrawable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gr.vaios.presentation.R;
import gr.vaios.presentation.model.GameModel;
import gr.vaios.presentation.presenter.GamesListPresenter;
import gr.vaios.presentation.ui.adapter.GamesListAdapter;
import gr.vaios.presentation.view.GamesListView;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class GamesListFragment extends BaseMvpFragment<GamesListView, GamesListPresenter>
        implements GamesListView {

    public static final String TAG = GamesListFragment.class.getName();

    @BindView(R.id.rvGames)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.btnBack)
    protected ImageButton mBack;

    @BindView(R.id.pbLoading)
    protected ProgressBar mLoadingIndicator;

    @BindView(R.id.tvError)
    protected TextView mErrorIndicator;

    private GamesListAdapter mAdapter;

    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_games_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public GamesListPresenter createPresenter() {
        return new GamesListPresenter();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Drawable backIcon = new IconicsDrawable(getContext())
                .icon(Entypo.Icon.ent_chevron_thin_left)
                .sizeDp(24);
        mBack.setImageDrawable(backIcon);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new GamesListAdapter();
        mAdapter.setOnItemListener(mItemListener);

        mRecyclerView.setAdapter(mAdapter);

        presenter.loadGames();
    }

    private GamesListAdapter.OnItemListener mItemListener = new GamesListAdapter.OnItemListener() {
        @Override
        public void onClick(GameModel game) {
            super.onClick(game);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag(GameDetailsFragment.TAG);
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            // Create and show the dialog.
            DialogFragment newFragment = GameDetailsFragment.newInstance(game);
            newFragment.show(ft, GameDetailsFragment.TAG);
        }
    };

    @Override
    public void showProgress() {
        mLoadingIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mLoadingIndicator.setVisibility(View.GONE);
    }

    @Override
    public void setGame(GameModel game) {
        mAdapter.addGame(game);
    }

    @Override
    public void showError(String msg) {
        mErrorIndicator.setText(msg);
    }

    @OnClick(R.id.btnBack)
    protected void goBack() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
