package gr.vaios.presentation.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gr.vaios.presentation.R;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public class NotSupportedFragment extends Fragment {

    public static final String TAG = NotSupportedFragment.class.getName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_not_supported, container, false);
    }

}
