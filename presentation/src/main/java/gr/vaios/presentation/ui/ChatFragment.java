package gr.vaios.presentation.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.mikepenz.entypo_typeface_library.Entypo;
import com.mikepenz.iconics.IconicsDrawable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gr.vaios.presentation.R;
import gr.vaios.presentation.model.MessageModel;
import gr.vaios.presentation.presenter.ChatPresenter;
import gr.vaios.presentation.ui.adapter.ChatAdapter;
import gr.vaios.presentation.view.ChatView;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class ChatFragment extends BaseMvpFragment<ChatView, ChatPresenter> implements ChatView {

    public static final String TAG = ChatFragment.class.getName();

    @BindView(R.id.rvMessages)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.etTextToSend)
    protected EditText mTextToSend;

    @BindView(R.id.btnSend)
    protected ImageButton mSend;

    private ChatAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public ChatPresenter createPresenter() {
        return new ChatPresenter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new ChatAdapter();
        mRecyclerView.setAdapter(mAdapter);

        Drawable backIcon = new IconicsDrawable(getContext())
                .icon(Entypo.Icon.ent_paper_plane)
                .sizeDp(24);
        mSend.setImageDrawable(backIcon);

        presenter.getHistory();
    }

    @Override
    public void setMessage(MessageModel message) {
        mAdapter.addMessage(message);
        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    @Override
    public void showError(String message) {
        //fixme
    }

    @Override
    public void hideProgress() {
        //fixme
    }

    @OnClick(R.id.btnSend)
    protected void sendMessage() {
        String msg = mTextToSend.getText().toString();
        presenter.sendMessage(msg);
        mTextToSend.setText("");
    }

}
