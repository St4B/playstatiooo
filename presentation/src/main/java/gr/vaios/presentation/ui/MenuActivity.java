package gr.vaios.presentation.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;

import com.mikepenz.entypo_typeface_library.Entypo;
import com.mikepenz.iconics.IconicsDrawable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gr.vaios.presentation.R;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class MenuActivity extends AppCompatActivity {

    @BindView(R.id.btnMainMenu)
    protected ImageButton mMainMenu;

    @BindView(R.id.btnShop)
    protected ImageButton mShop;

    @BindView(R.id.btnChat)
    protected ImageButton mChat;

    @BindView(R.id.btnPlay)
    protected ImageButton mPlay;

    @BindView(R.id.btnProfile)
    protected ImageButton mProfile;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        Drawable keyboartIcon = new IconicsDrawable(this)
                .icon(Entypo.Icon.ent_keyboard)
                .sizeDp(24);
        mMainMenu.setImageDrawable(keyboartIcon);

        Drawable cartIcon = new IconicsDrawable(this)
                .icon(Entypo.Icon.ent_shopping_cart)
                .sizeDp(24);
        mShop.setImageDrawable(cartIcon);

        Drawable chatIcon = new IconicsDrawable(this)
                .icon(Entypo.Icon.ent_chat)
                .sizeDp(24);
        mChat.setImageDrawable(chatIcon);

        Drawable playIcon = new IconicsDrawable(this)
                .icon(Entypo.Icon.ent_controller_play)
                .sizeDp(24);
        mPlay.setImageDrawable(playIcon);

        Drawable listIcon = new IconicsDrawable(this)
                .icon(Entypo.Icon.ent_list)
                .sizeDp(24);
        mProfile.setImageDrawable(listIcon);

        if (savedInstanceState == null) {
            selectProfile();
        }
    }

    @OnClick(R.id.btnMainMenu)
    public void selectMainMenu() {
        addNoSupportedFragment();
    }

    @OnClick(R.id.btnShop)
    public void selectShop() {
        addNoSupportedFragment();
    }

    @OnClick(R.id.btnChat)
    public void selectChat() {
        String tag = ChatFragment.TAG;

        Fragment fragment = new ChatFragment();
        goToTab(fragment, tag);
    }

    @OnClick(R.id.btnPlay)
    public void selectPlay() {
        addNoSupportedFragment();
    }

    @OnClick(R.id.btnProfile)
    public void selectProfile() {
        String tag = ProfileFragment.TAG;

        Fragment fragment = new ProfileFragment();
        goToRoot(fragment, tag);
    }

    private void goToTab(Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.container, fragment, tag);
        transaction.setReorderingAllowed(true);
        transaction.commit();
    }

    private void goToRoot(Fragment fragment, String tag) {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    private void addNoSupportedFragment() {
        String tag = NotSupportedFragment.TAG;

        Fragment fragment = new NotSupportedFragment();
        goToTab(fragment, tag);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
