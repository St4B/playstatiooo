package gr.vaios.presentation.ui.adapter;

import static gr.vaios.presentation.ResourcesUtils.getResId;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import gr.vaios.presentation.R;
import gr.vaios.presentation.model.GameModel;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class GamesListAdapter extends RecyclerView.Adapter<GamesListAdapter.GameItemViewHolder> {

    private OnItemListener mOnItemListener = new OnItemListener() {};

    private ArrayList<GameModel> mGames = new ArrayList<>();

    public GamesListAdapter() {}

    public void addGame(GameModel game) {
        mGames.add(game);
        notifyDataSetChanged();
    }

    @Override
    public GameItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_games_item, parent, false);

        GameItemViewHolder viewHolder = new GameItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GameItemViewHolder viewHolder, int position) {
        GameModel game = mGames.get(position);

        viewHolder.mTitle.setText(game.getTitle());
        viewHolder.itemView.setOnClickListener(view -> mOnItemListener.onClick(game));

        int resId = getResId(game.getImage(), R.drawable.class);
        if (resId != -1 ) {
            viewHolder.mPicture.setImageResource(resId);
        }

        viewHolder.mProgressBar.setProgress(game.getPercentage().intValue());
        viewHolder.mPercentage.setText(game.getPercentage().toPlainString() + "%");
    }

    @Override
    public int getItemCount() {
        return mGames.size();
    }

    protected static class GameItemViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;

        private ImageView mPicture;

        private ProgressBar mProgressBar;

        private TextView mPercentage;

        private GameItemViewHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.tvTitle);
            mPicture = view.findViewById(R.id.ivPicture);
            mProgressBar = view.findViewById(R.id.pbPercentage);
            mPercentage = view.findViewById(R.id.tvPercentage);
        }

    }

    public void setOnItemListener(OnItemListener listener) {
        if (listener != null) mOnItemListener = listener;
    }

    public static abstract class OnItemListener {

        public void onClick(GameModel game) {}

    }

}
