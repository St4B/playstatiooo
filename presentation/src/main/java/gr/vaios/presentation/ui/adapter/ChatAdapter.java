package gr.vaios.presentation.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import java.util.ArrayList;

import gr.vaios.presentation.R;
import gr.vaios.presentation.model.MessageModel;

/**
 * Created by v.tsitsonis on 2/3/2018.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatItemViewHolder> {

    private final LayoutParams mMyLayoutParams;

    private final LayoutParams mOthersLayoutParams;

    private ArrayList<MessageModel> mMessages = new ArrayList<>();

    public ChatAdapter() {
        mMyLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mMyLayoutParams.gravity = Gravity.START;

        mOthersLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mOthersLayoutParams.gravity = Gravity.END;
    }

    public void addMessage(MessageModel message) {
        mMessages.add(message);
        notifyDataSetChanged();
    }

    @Override
    public ChatItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_messages_item, parent, false);

        ChatItemViewHolder viewHolder = new ChatItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChatItemViewHolder holder, int position) {
        MessageModel message = mMessages.get(position);

        String user = message.getUser();

        LayoutParams params;
        if (MessageModel.sMyName.equalsIgnoreCase(user)) {
            params = mMyLayoutParams;
            holder.mText.setBackgroundResource(R.drawable.rounded_corners_green);
        } else {
            params = mOthersLayoutParams;
            holder.mText.setBackgroundResource(R.drawable.rounded_corners_pink);
        }

        holder.mUser.setText(message.getUser());
        holder.mUser.setLayoutParams(params);
        holder.mText.setText(message.getText());
        holder.mText.setLayoutParams(params);
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    protected static class ChatItemViewHolder extends RecyclerView.ViewHolder {

        private TextView mText;

        private TextView mUser;

        private ChatItemViewHolder(View view) {
            super(view);
            mText = view.findViewById(R.id.tvText);
            mUser = view.findViewById(R.id.tvUser);
        }

    }

}
