package gr.vaios.presentation.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import gr.vaios.presentation.R;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class ProfileFragment  extends AppCompatDialogFragment {

    public static final String TAG = ProfileFragment.class.getName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btnTrophies)
    protected void goToTrophies() {
        GamesListFragment fragment = new GamesListFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(TAG);
        transaction.commit();
    }
    
}
