package gr.vaios.presentation.dependencyInjection.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import gr.vaios.data.executor.JobExecutor;
import gr.vaios.domain.executor.PostExecutionThread;
import gr.vaios.domain.executor.ThreadExecutor;
import gr.vaios.presentation.UIThread;

/**
 * Created by Vaios on 9/25/17.
 */
@Module
public class AppModule {

    @Provides @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }
}
