package gr.vaios.presentation.dependencyInjection.component;

import javax.inject.Singleton;

import dagger.Component;
import gr.vaios.domain.executor.PostExecutionThread;
import gr.vaios.domain.executor.ThreadExecutor;
import gr.vaios.presentation.dependencyInjection.module.AppModule;

/**
 * Created by Vaios on 9/25/17.
 */
@Singleton
@Component (modules = AppModule.class)
public interface AppComponent {

    //Exposed to sub-graphs.
    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();

}
