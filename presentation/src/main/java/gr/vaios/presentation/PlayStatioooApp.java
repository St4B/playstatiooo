package gr.vaios.presentation;

import android.app.Application;
import android.content.Context;

//import gr.vaios.presentation.dependencyInjection.component.AppComponent;
//import gr.vaios.presentation.dependencyInjection.component.DaggerAppComponent;

/**
 * Created by Vaios on 9/25/17.
 */
public class PlayStatioooApp extends Application {

//    private AppComponent mAppComponent;

    private static Context sContext;

    @Override public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
      //  mAppComponent = DaggerAppComponent.builder().build();
    }

    public static Context getContext() {
        return sContext;
    }
}
