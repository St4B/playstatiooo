package gr.vaios.presentation.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import gr.vaios.presentation.model.MessageModel;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public interface ChatView extends MvpView {

    void setMessage(MessageModel message);

    void showError(String message);

    void hideProgress();
}
