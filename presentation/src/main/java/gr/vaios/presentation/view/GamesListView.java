package gr.vaios.presentation.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import gr.vaios.presentation.model.GameModel;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public interface GamesListView extends MvpView {

    void showProgress();

    void hideProgress();

    void setGame(GameModel game);

    void showError(String msg);

}
