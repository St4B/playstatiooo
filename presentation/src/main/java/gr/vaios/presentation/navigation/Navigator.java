package gr.vaios.presentation.navigation;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 *  <p>This class is responsible for the navigation through the Application. In every activity we must
 *  implement a public static method that returns the Intent which Navigator is going to use in order
 *  to start the specific activity. By having the Intent creation in each activity, we make it less
 *  easier to forget to pass important values in the 'Extras'.
 *  </p>
 *  <p>Handling the start of every activity through Navigator we guarantee that implemented method
 *  is going to be called in order to start the activity. In other case navigation may become
 *  inconsistent and less maintainable.
 *  </p>
 *
 * Created by Vaios on 9/23/17.
 *
 */
@Singleton
public class Navigator {

    @Inject
    public Navigator(){}

}
