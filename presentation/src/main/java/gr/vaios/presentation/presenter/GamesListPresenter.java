package gr.vaios.presentation.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import gr.vaios.data.executor.JobExecutor;
import gr.vaios.data.repository.GamesListRepository;
import gr.vaios.data.storage.DiskStorage;
import gr.vaios.data.storage.IStorage;
import gr.vaios.data.storage.serialize.GsonSerializer;
import gr.vaios.data.storage.serialize.ISerializer;
import gr.vaios.domain.executor.PostExecutionThread;
import gr.vaios.domain.executor.ThreadExecutor;
import gr.vaios.domain.interactor.GetGamesUseCase;
import gr.vaios.domain.module.GameModule;
import gr.vaios.domain.repository.IGamesListRepository;
import gr.vaios.presentation.PlayStatioooApp;
import gr.vaios.presentation.UIThread;
import gr.vaios.presentation.mapper.GameModelDataMapper;
import gr.vaios.presentation.model.GameModel;
import gr.vaios.presentation.view.GamesListView;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class GamesListPresenter extends MvpBasePresenter<GamesListView> {

    private final ISerializer mSerializer = new GsonSerializer();

    private final IStorage mStorage;

    private final IGamesListRepository mGamesListRepository;

    private final ThreadExecutor mExecutor = new JobExecutor();

    private final PostExecutionThread mPostExecutionThread = new UIThread();

    private final GetGamesUseCase mGetGamesUseCase;

    private final GameModelDataMapper mGameModelDataMapper = new GameModelDataMapper();

    public GamesListPresenter() {
        mStorage = new DiskStorage(PlayStatioooApp.getContext(), mSerializer);
        mGamesListRepository = new GamesListRepository(mStorage);
        mGetGamesUseCase = new GetGamesUseCase(mGamesListRepository, mExecutor, mPostExecutionThread);
    }

    public void loadGames() {
        getView().showProgress();
        mGetGamesUseCase.execute(new LoadGamesObserver(), null);
    }

    private class LoadGamesObserver extends DisposableObserver<GameModule> {


        @Override
        public void onNext(GameModule module) {
            if (isViewAttached()) {
                GameModel game = mGameModelDataMapper.transform(module);
                getView().setGame(game);
            }
        }

        @Override
        public void onError(Throwable e) {
            if (isViewAttached()) {
                getView().showError(e.getMessage());
                getView().hideProgress();
            }
        }

        @Override
        public void onComplete() {
            if (isViewAttached()) {
                getView().hideProgress();
            }
        }
    }

}
