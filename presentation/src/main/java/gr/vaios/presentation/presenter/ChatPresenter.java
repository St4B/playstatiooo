package gr.vaios.presentation.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import gr.vaios.data.executor.JobExecutor;
import gr.vaios.data.repository.MessagesRepository;
import gr.vaios.data.storage.IStorage;
import gr.vaios.data.storage.PreferencesStorage;
import gr.vaios.data.storage.serialize.GsonSerializer;
import gr.vaios.data.storage.serialize.ISerializer;
import gr.vaios.domain.executor.PostExecutionThread;
import gr.vaios.domain.executor.ThreadExecutor;
import gr.vaios.domain.interactor.GetMessageHistoryUseCase;
import gr.vaios.domain.interactor.SendMessageUseCase;
import gr.vaios.domain.module.MessageModule;
import gr.vaios.domain.repository.IMessagesRepository;
import gr.vaios.presentation.PlayStatioooApp;
import gr.vaios.presentation.UIThread;
import gr.vaios.presentation.mapper.MessageModelDataMapper;
import gr.vaios.presentation.model.MessageModel;
import gr.vaios.presentation.view.ChatView;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public class ChatPresenter extends MvpBasePresenter<ChatView> {

    private final ISerializer mSerializer = new GsonSerializer();

    private final IStorage mStorage;

    private final IMessagesRepository mMessagesRepository;

    private final ThreadExecutor mExecutor = new JobExecutor();

    private final PostExecutionThread mPostExecutionThread = new UIThread();

    private final GetMessageHistoryUseCase mGetMessageHistoryUseCase;

    private final SendMessageUseCase mSendMessageUseCase;

    private final MessageModelDataMapper mMessageModelDataMapper = new MessageModelDataMapper();

    public ChatPresenter() {
        mStorage = new PreferencesStorage(PlayStatioooApp.getContext(), mSerializer);
        mMessagesRepository = new MessagesRepository(mStorage);
        mGetMessageHistoryUseCase = new GetMessageHistoryUseCase(mMessagesRepository, mExecutor,
                mPostExecutionThread);
        mSendMessageUseCase = new SendMessageUseCase(mMessagesRepository, mExecutor,
                mPostExecutionThread);
    }

    public void getHistory() {
        mGetMessageHistoryUseCase.execute(new GetMessageHistoryObserver(), null);
    }

    protected class GetMessageHistoryObserver extends DisposableObserver<MessageModule> {

        @Override
        public void onNext(MessageModule module) {
            MessageModel message = mMessageModelDataMapper.transform(module);
            if (isViewAttached()) {
                getView().setMessage(message);
            }
        }

        @Override
        public void onError(Throwable e) {
            if (isViewAttached()) {
                getView().showError(e.getMessage());
                getView().hideProgress();
            }
        }

        @Override
        public void onComplete() {
            if (isViewAttached()) {
                getView().hideProgress();
            }
        }
    }

    public void sendMessage(String msg) {
        mSendMessageUseCase.execute(new SendMessageObserver(),
                SendMessageUseCase.Params.forMessage(msg, MessageModel.sMyName));
    }

    protected final class SendMessageObserver extends DisposableObserver<MessageModule> {

        @Override
        public void onNext(MessageModule module) {
            MessageModel message = mMessageModelDataMapper.transform(module);
            if (isViewAttached()) {
                getView().setMessage(message);
            }
        }

        @Override
        public void onError(Throwable e) {
            if (isViewAttached()) {
                getView().showError(e.getMessage());
                getView().hideProgress();
            }
        }

        @Override
        public void onComplete() {

        }
    }

}
