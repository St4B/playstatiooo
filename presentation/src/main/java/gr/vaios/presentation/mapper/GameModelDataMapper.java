package gr.vaios.presentation.mapper;

import gr.vaios.domain.module.GameModule;
import gr.vaios.presentation.model.GameModel;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class GameModelDataMapper {

    public GameModel transform(GameModule module) {
        GameModel.Builder builder = new GameModel.Builder();

        builder.title(module.getTitle())
                .percentage(module.getPercentage())
                .image(module.getImage())
                .bronze(module.getBronze())
                .silver(module.getSilver())
                .golden(module.getGolden())
                .blue(module.getBlue())
                .total(module.getTotal());

        return builder.build();
    }
}
