package gr.vaios.presentation.mapper;

import gr.vaios.domain.module.MessageModule;
import gr.vaios.presentation.model.MessageModel;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public class MessageModelDataMapper {

    public MessageModule transform(MessageModel model) {
        MessageModule.Builder builder = new MessageModule.Builder();

        builder.text(model.getText())
                .user(model.getUser());

        return builder.build();
    }

    public MessageModel transform(MessageModule module) {
        MessageModel.Builder builder = new MessageModel.Builder();

        builder.text(module.getText())
                .user(module.getUser());

        return builder.build();
    }

}
