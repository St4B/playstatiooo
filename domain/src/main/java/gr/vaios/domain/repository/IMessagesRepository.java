package gr.vaios.domain.repository;

import gr.vaios.domain.module.MessageModule;
import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public interface IMessagesRepository {

    Observable<MessageModule> getHistory();

    Completable sendMessage(MessageModule message);

}
