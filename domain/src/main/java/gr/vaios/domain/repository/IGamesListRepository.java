package gr.vaios.domain.repository;

import gr.vaios.domain.module.GameModule;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public interface IGamesListRepository {

    Observable<GameModule> getGames();

}
