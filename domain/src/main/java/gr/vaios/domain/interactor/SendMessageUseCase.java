package gr.vaios.domain.interactor;

import gr.vaios.domain.executor.PostExecutionThread;
import gr.vaios.domain.executor.ThreadExecutor;
import gr.vaios.domain.module.MessageModule;
import gr.vaios.domain.repository.IMessagesRepository;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public class SendMessageUseCase extends UseCase<MessageModule, SendMessageUseCase.Params> {

    private final IMessagesRepository mMessagesRepository;

    public SendMessageUseCase(IMessagesRepository messagesRepository,
            ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mMessagesRepository = messagesRepository;
    }

    @Override
    public Observable<MessageModule> buildUseCaseObservable(Params params) {
        MessageModule.Builder builder = new MessageModule.Builder();
        builder.user(params.mUser)
                .text(params.mText);

        MessageModule message = builder.build();

        return mMessagesRepository.sendMessage(message).andThen(Observable.just(message));
    }

    public static final class Params {

        private final String mText;

        private final String mUser;

        private Params(String text, String user) {
            mText = text;
            mUser = user;
        }

        public static Params forMessage(String text, String user) {
            return new Params(text, user);
        }

    }

}
