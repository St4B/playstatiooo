package gr.vaios.domain.interactor;

import gr.vaios.domain.executor.PostExecutionThread;
import gr.vaios.domain.executor.ThreadExecutor;
import gr.vaios.domain.module.MessageModule;
import gr.vaios.domain.repository.IMessagesRepository;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public class GetMessageHistoryUseCase extends UseCase<MessageModule, GetMessageHistoryUseCase.Params> {

    private final IMessagesRepository mMessagesRepository;

    public GetMessageHistoryUseCase(IMessagesRepository messagesRepository ,
            ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mMessagesRepository = messagesRepository;
    }

    @Override
    public Observable<MessageModule> buildUseCaseObservable(Params params) {
        return mMessagesRepository.getHistory();
    }

    public static final class Params {}

}
