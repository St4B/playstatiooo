package gr.vaios.domain.interactor;

import gr.vaios.domain.executor.PostExecutionThread;
import gr.vaios.domain.executor.ThreadExecutor;
import gr.vaios.domain.module.GameModule;
import gr.vaios.domain.interactor.GetGamesUseCase.Params;
import gr.vaios.domain.repository.IGamesListRepository;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class GetGamesUseCase extends UseCase<GameModule, Params> {

    private final IGamesListRepository mGamesListRepository;

    public GetGamesUseCase(IGamesListRepository gamesListRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mGamesListRepository = gamesListRepository;
    }

    @Override
    public Observable<GameModule> buildUseCaseObservable(Params params) {
        return mGamesListRepository.getGames();
    }

    public static final class Params {}

}
