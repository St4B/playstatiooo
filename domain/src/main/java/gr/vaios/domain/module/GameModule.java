package gr.vaios.domain.module;

import java.math.BigDecimal;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public final class GameModule {

    private final String mTitle;

    private final BigDecimal mPercentage;

    private final String mImage;

    private final BigDecimal mBronze;

    private final BigDecimal mSilver;

    private final BigDecimal mGolden;

    private final BigDecimal mBlue;

    private final BigDecimal mTotal;

    private GameModule(Builder builder) {
        mTitle = builder.mTitle;
        mPercentage = builder.mPercentage;
        mImage = builder.mImage;
        mBronze = builder.mBronze;
        mSilver = builder.mSilver;
        mGolden = builder.mGolden;
        mBlue = builder.mBlue;
        mTotal = builder.mTotal;
    }

    public String getTitle() {
        return mTitle;
    }

    public BigDecimal getPercentage() {
        return mPercentage;
    }

    public String getImage() {
        return mImage;
    }

    public BigDecimal getBronze() {
        return mBronze;
    }

    public BigDecimal getSilver() {
        return mSilver;
    }

    public BigDecimal getGolden() {
        return mGolden;
    }

    public BigDecimal getBlue() {
        return mBlue;
    }

    public BigDecimal getTotal() {
        return mTotal;
    }

    public static final class Builder {

        private String mTitle = "";

        private BigDecimal mPercentage = BigDecimal.ZERO;

        private String mImage = "";

        private BigDecimal mBronze = BigDecimal.ZERO;

        private BigDecimal mSilver = BigDecimal.ZERO;

        private BigDecimal mGolden = BigDecimal.ZERO;

        private BigDecimal mBlue = BigDecimal.ZERO;

        private BigDecimal mTotal = BigDecimal.ZERO;

        public Builder title(String title) {
            mTitle = title;
            return this;
        }

        public Builder percentage(BigDecimal percentage) {
            mPercentage = percentage;
            return this;
        }

        public Builder image(String image) {
            mImage = image;
            return this;
        }

        public Builder bronze(BigDecimal bronze) {
            mBronze = bronze;
            return this;
        }

        public Builder silver(BigDecimal silver) {
            mSilver = silver;
            return this;
        }

        public Builder golden(BigDecimal golden) {
            mGolden = golden;
            return this;
        }

        public Builder blue(BigDecimal blue) {
            mBlue = blue;
            return this;
        }

        public Builder total(BigDecimal total) {
            mTotal = total;
            return this;
        }

        public GameModule build() {
            return new GameModule(this);
        }

    }
}
