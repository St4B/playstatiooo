package gr.vaios.domain.module;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public final class MessageModule {

    private final String mText;

    private final String mUser;

    private MessageModule(Builder builder) {
        mText = builder.mText;
        mUser = builder.mUser;
    }

    public String getText() {
        return mText;
    }

    public String getUser() {
        return mUser;
    }

    public static final class Builder {

        private String mText;

        private String mUser;

        public Builder text(String text) {
            mText = text;
            return this;
        }

        public Builder user(String user) {
            mUser = user;
            return this;
        }

        public MessageModule build () {
            return new MessageModule(this);
        }

    }

}
