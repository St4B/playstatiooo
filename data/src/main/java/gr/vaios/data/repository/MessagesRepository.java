package gr.vaios.data.repository;

import gr.vaios.data.mapper.MessageEntityDataMapper;
import gr.vaios.data.storage.IStorage;
import gr.vaios.domain.module.MessageModule;
import gr.vaios.domain.repository.IMessagesRepository;
import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public class MessagesRepository implements IMessagesRepository {

    private final IStorage mStorage;

    private final MessageEntityDataMapper mMessageEntityDataMapper = new MessageEntityDataMapper();

    public MessagesRepository(IStorage storage) {
        mStorage = storage;
    }

    @Override
    public Observable<MessageModule> getHistory() {
        return mStorage.loadMessageHistory().map(mMessageEntityDataMapper::transform);
    }

    @Override
    public Completable sendMessage(MessageModule message) {
        return mStorage.addMessage(mMessageEntityDataMapper.transform(message));
    }
}
