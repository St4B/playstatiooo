package gr.vaios.data.repository;

import gr.vaios.data.mapper.GameEntityDataMapper;
import gr.vaios.data.storage.IStorage;
import gr.vaios.domain.module.GameModule;
import gr.vaios.domain.repository.IGamesListRepository;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class GamesListRepository implements IGamesListRepository {
    
    private final IStorage mStorage;
    
    private final GameEntityDataMapper mGameEntityDataMapper = new GameEntityDataMapper();
    
    public GamesListRepository(IStorage storage) {
        mStorage = storage;
    }

    @Override
    public Observable<GameModule> getGames() {
        return mStorage.loadGames().map(mGameEntityDataMapper::transform);
    }
}
