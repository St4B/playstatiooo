package gr.vaios.data.entity;

import java.util.ArrayList;

/**
 *
 * Workaround for linkedTreeMap deserialization
 *
 * Created by v.tsitsonis on 3/3/2018.
 */

public final class MessageEntities extends ArrayList<MessageEntity> {
}

