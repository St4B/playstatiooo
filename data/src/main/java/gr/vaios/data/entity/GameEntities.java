package gr.vaios.data.entity;

import java.util.ArrayList;

/**
 *
 * Workaround for linkedTreeMap deserialization
 *
 * Created by v.tsitsonis on 15/2/2018.
 */

public final class GameEntities extends ArrayList<GameEntity> {
}
