package gr.vaios.data.mapper;

import gr.vaios.data.entity.GameEntity;
import gr.vaios.domain.module.GameModule;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class GameEntityDataMapper {

    public GameModule transform(GameEntity entity) {
        GameModule.Builder builder = new GameModule.Builder();

        builder.title(entity.getTitle())
                .percentage(entity.getPercentage())
                .image(entity.getImage())
                .bronze(entity.getBronze())
                .silver(entity.getSilver())
                .golden(entity.getGolden())
                .blue(entity.getBlue())
                .total(entity.getTotal());

        return builder.build();
    }
}
