package gr.vaios.data.mapper;

import gr.vaios.data.entity.MessageEntity;
import gr.vaios.domain.module.MessageModule;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public class MessageEntityDataMapper {

    public MessageModule transform(MessageEntity entity) {
        MessageModule.Builder builder = new MessageModule.Builder();

        builder.text(entity.getText())
                .user(entity.getUser());

        return builder.build();
    }

    public MessageEntity transform(MessageModule module) {
        MessageEntity.Builder builder = new MessageEntity.Builder();

        builder.text(module.getText())
                .user(module.getUser());

        return builder.build();
    }
}
