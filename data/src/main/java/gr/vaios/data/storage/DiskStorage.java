package gr.vaios.data.storage;

import android.content.Context;
import android.content.res.Resources;

import java.io.InputStream;
import java.util.ArrayList;

import gr.vaios.data.R;
import gr.vaios.data.entity.GameEntities;
import gr.vaios.data.entity.GameEntity;
import gr.vaios.data.entity.MessageEntity;
import gr.vaios.data.storage.serialize.ISerializer;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.Emitter;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class DiskStorage implements IStorage {

    private final Context mContext;

    private final ISerializer mSerializer;

    public DiskStorage(Context context, ISerializer serializer) {
        mContext = context.getApplicationContext();
        mSerializer = serializer;
    }

    @Override
    public Observable<GameEntity> loadGames() {
        return Observable.create(subscriber -> {
                    try {
                        Resources res = mContext.getResources();
                        InputStream is = res.openRawResource(R.raw.games);

                        byte[] bytes = new byte[is.available()];
                        is.read(bytes);
                        String json = new String(bytes);

                        ArrayList<GameEntity> entities = mSerializer.deserialize(json,
                                GameEntities.class);

                        for (GameEntity entity : entities) {
                            subscriber.onNext(entity);
                        }
                        subscriber.onComplete();
                    } catch (Exception e) {
                        subscriber.onError(e);
                    }
                }
        );
    }

    @Override
    public Observable<MessageEntity> loadMessageHistory() {
        return Observable.create(Emitter::onComplete);
    }

    @Override
    public Completable addMessage(MessageEntity message) {
        return Completable.create(CompletableEmitter::onComplete);
    }

}
