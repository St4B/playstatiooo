package gr.vaios.data.storage;

import gr.vaios.data.entity.GameEntity;
import gr.vaios.data.entity.MessageEntity;
import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public interface IStorage {

    Observable<GameEntity> loadGames();

    Observable<MessageEntity> loadMessageHistory();

    Completable addMessage(MessageEntity message);

}
