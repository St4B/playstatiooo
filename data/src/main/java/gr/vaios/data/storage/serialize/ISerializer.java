package gr.vaios.data.storage.serialize;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public interface ISerializer {

    String serialize(Object object, Class clazz);

    <T> T deserialize(String string, Class<T> clazz);
}
