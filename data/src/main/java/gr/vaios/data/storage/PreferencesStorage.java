package gr.vaios.data.storage;

import android.content.Context;
import android.content.SharedPreferences;

import gr.vaios.data.entity.GameEntity;
import gr.vaios.data.entity.MessageEntities;
import gr.vaios.data.entity.MessageEntity;
import gr.vaios.data.storage.serialize.ISerializer;
import io.reactivex.Completable;
import io.reactivex.Emitter;
import io.reactivex.Observable;

/**
 * Created by v.tsitsonis on 3/3/2018.
 */

public class PreferencesStorage implements IStorage {

    private final Context mContext;

    private final ISerializer mSerializer;

    private final SharedPreferences mPreferences;

    private static final String sFileName = "messages";

    private static final String KEY_MESSAGES = "messages";

    public PreferencesStorage(Context context, ISerializer serializer) {
        mContext = context.getApplicationContext();
        mPreferences = mContext.getSharedPreferences(sFileName, Context.MODE_PRIVATE);
        mSerializer = serializer;
    }

    @Override
    public Observable<GameEntity> loadGames() {
        return Observable.create(Emitter::onComplete);
    }

    @Override
    public Observable<MessageEntity> loadMessageHistory() {
        return Observable.create(subscriber -> {
                    try {
                        String serializedMessages = mPreferences.getString(KEY_MESSAGES, "");
                        MessageEntities entities =
                                mSerializer.deserialize(serializedMessages, MessageEntities.class);

                        if (entities == null) {
                            entities = getInitialMessages();
                            storeMessages(entities);
                        }

                        for (MessageEntity entity : entities) {
                            subscriber.onNext(entity);
                        }
                        subscriber.onComplete();
                    } catch (Exception e) {
                        subscriber.onError(e);
                    }
                }
        );
    }

    private MessageEntities getInitialMessages() {
        MessageEntities messages = new MessageEntities();

        MessageEntity.Builder builder = new MessageEntity.Builder();
        builder.text("Hello Bro!").user("Alex");
        messages.add(builder.build());

        builder.text("Wazzap?");
        messages.add(builder.build());

        builder.text("Fine! U?").user("Me");
        messages.add(builder.build());

        builder.text("I'm cool!").user("Alex");
        messages.add(builder.build());

        builder.text("Great! :D").user("Me");
        messages.add(builder.build());

        return messages;
    }


    @Override
    public Completable addMessage(MessageEntity message) {
        return Completable.create(subscriber -> {
                    try {
                        String serializedMessages = mPreferences.getString(KEY_MESSAGES, "");
                        MessageEntities entities =
                                mSerializer.deserialize(serializedMessages, MessageEntities.class);

                        entities.add(message);
                        storeMessages(entities);

                        subscriber.onComplete();
                    } catch (Exception e) {
                        subscriber.onError(e);
                    }
                }
        );
    }

    private void storeMessages(MessageEntities entities) {
        String serializedMessages = mSerializer.serialize(entities, MessageEntities.class);
        mPreferences.edit().putString(KEY_MESSAGES, serializedMessages).commit();
    }

}
