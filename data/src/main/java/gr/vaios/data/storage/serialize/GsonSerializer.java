package gr.vaios.data.storage.serialize;

import com.google.gson.Gson;

/**
 * Created by v.tsitsonis on 15/2/2018.
 */

public class GsonSerializer implements ISerializer {

    private final Gson mGson = new Gson();

    @Override
    public String serialize(Object object, Class clazz) {
        return mGson.toJson(object, clazz);
    }

    @Override
    public <T> T deserialize(String string, Class<T> clazz) {
        return mGson.fromJson(string, clazz);
    }

}
